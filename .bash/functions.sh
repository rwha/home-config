
# Show file info for a command found in PATH; show and follow symlinks.
binfo() {
  [ -z $1 ] && {
    echo "Usage: binfo command";
    exit;
  }

  local file=$(which $1)

  # check for symlink and follow it
  [ -h $file ] && {
    echo "$file is a symbolic link to $(readlink -m $file)";
    file=$(readlink -m $file);
  }

  [ -a $file ] && file $file || echo $file
}

# Try to open the file if it's entered as a command
function command_not_found_handle {
	local file=$1
	local ext="${file##*.}"
	for dir in docs downloads desktop templates; do
		[[ -f ~/${dir}/${file} ]] && file=~/${dir}/${file} && break
	done

	[[ -f "${file}" ]] && [[ -n "${ext}"  ]] || {
		echo "bash: ${1}: command not found"
		exit 127 &>/dev/null
	}

	case $ext in
		pdf)
			exec zathura ${file} &
			;;
		html)
			exec firefox ${file} &
			;;
		*)
			echo "bash: ${1}: unkown filetype: ${ext}"
			exit 127 &>/dev/null
			;;
	esac
}
