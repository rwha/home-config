# ~/.bashrc

[[ $- != *i* ]] && return

HISTCONTROL=ignoreboth
HISTSIZE=-1
HISTFILE=~/.history
HISTIGNORE=ls:ll:la:lh:top:df*:du*:cd

export WLC_DRM_DEVICE=card1

[[ -d ~/.bash ]] && {
	for f in ~/.bash/*.sh ; do
		[[ -r "${f}" ]] && . "${f}"
	done
	unset f
}
