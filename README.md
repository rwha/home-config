## Personal configuration files and utility scripts

[Sway](https://github.com/SirCmpwn/sway) config and custom status command (python 3)

_NB: the status command relies on a [patched font](https://github.com/ryanoasis/nerd-fonts) and was written for my own laptop, don't expect it to work for you!_

Vim config, using
 - [powerline](https://github.com/powerline/powerline)
 - [Vundle](https://github.com/VundleVim/Vundle.vim)
 - [nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
 - [vim-workspace](https://github.com/bagrat/vim-workspace)
 - [vim-devicons](https://github.com/ryanoasis/vim-devicons)


Bash config. Perhaps unusually organized, but I like it (for now).


various utility and helper scripts.


